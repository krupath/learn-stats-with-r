# learn-stats-with-r



# Description

This repo tracks my progress through the 'Learning  statistics with R' book by Dr. Danielle Navarro. 

# Sources

- Website: https://learningstatisticswithr.com/ 
- Github: https://github.com/djnavarro/rbook 

# Progress 
Each milestone corresponds to a chapter in the book, as listed beloW:

I. Background

    Chapter 1: Why do we learn statistics? Psychology and statistics. Statistics in everyday life. Some examples where intuition is misleading, and statistics is critical.
    Chapter 2: A brief introduction to research design. Basics of psychological measurement. Reliability and validity of a measurement. Experimental and non-experimental design. Predictors versus outcomes.

II. An introduction to R

    Chapter 3: Getting started with R. Getting R and Rstudio. Typing commands at the console. Simple calculations. Using functions. Introduction to variables. Numeric, character and logical data. Storing multiple values as a vector.
    Chapter 4: Additional R concepts. Installing and loading packages. The workspace. Navigating the file system. More complicated data structures: factors, data frames, lists and formulas. A brief discussion of generic functions.

III. Working with data

    Chapter 5: Descriptive statistics. Mean, median and mode. Range, interquartile range and standard deviations. Skew and kurtosis. Standard scores. Correlations. Tools for computing these things in R. Brief comments missing data.
    Chapter 6: Drawing graphs. Discussion of R graphics. Histograms. Stem and leaf plots. Boxplots. Scatterplots. Bar graphs.
    Chapter 7: Pragmatic matters. Tabulating data. Transforming a variable. Subsetting vectors and data frames. Sorting, transposing and merging data. Reshaping a data frame. Basics of text processing. Reading unusual data files. Basics of variable coercion. Even more data structures. Other miscellaneous topics, including floating point arithmetic.
    Chapter 8: Basic programming. Scripts. Loops. Conditionals. Writing functions. Implicit loops.

IV. Statistical theory

    Prelude. The riddle of induction, and why statisticians make assumptions.
    Chapter 9: Introduction to probability. Probability versus statistics. Basics of probability theory. Common distributions: normal, binomial, t, chi-square, F. Bayesian versus frequentist probability.
    Chapter 10: Estimating unknown quantities from a sample. Sampling from populations. Estimating population means and standard deviations. Sampling distributions. The central limit theorem. Confidence intervals.
    Chapter 11: Hypothesis testing. Research hypotheses versus statistical hypotheses. Null versus alternative hypotheses. Type I and Type II errors. Sampling distributions for test statistics. Hypothesis testing as decision making. p-values. Reporting the results of a test. Effect size and power. Controversies and traps in hypothesis testing.

V. Statistical tools

    Chapter 12: Categorical data analysis. Chi-square goodness of fit test. Chi-square test of independence. Yate’s continuity correction. Effect size with Cramer’s V. Assumptions of the tests. Other tests: Fisher exact test and McNemar’s test.
    Chapter 13: Comparing two means. One sample z-test. One sample t-test. Student’s independent sample t-test. Welch’s independent samples t-test. Paired sample t-test. Effect size with Cohen’s d. Checking the normality assumption. Wilcoxon tests for non-normal data.
    Chapter 14: Comparing several means (one-way ANOVA). Introduction to one-way ANOVA. Doing it in R. Effect size with eta-squared. Simple corrections for multiple comparisons (post hoc tests). Assumptions of one-way ANOVA. Checking homogeneity of variance using Levene tests. Avoiding the homogeneity of variance assumption. Checking and avoiding the normality assumption. Relationship between ANOVA and t-tests.
    Chapter 15: Linear regression. Introduction to regression. Estimation by least squares. Multiple regression models. Measuring the fit of a regression model. Hypothesis tests for regression models. Standardised regression coefficient. Assumptions of regression models. Basic regression diagnostics. Model selection methods for regression.
    Chapter 16: Factorial ANOVA. Factorial ANOVA without interactions. Factorial ANOVA with interactions. Effect sizes, estimated marginal means, confidence intervals for effects. Assumption checking. F-tests as model selection. Interpreting ANOVA as a linear model. Specifying contrasts. Post hoc testing via Tukey’s HSD. Factorial ANOVA with unbalanced data (Type I, III and III sums of squares)

VI. Other topics

    Chapter 17: Bayesian statistics. Introduction to Bayesian inference. Bayesian analysis of contingency tables. Bayesian t-tests, ANOVAs and regressions.
    Chapter 18: Epilogue. Comments on the content missing from this book. Advantages to using R.
    References. An incomplete reference list.

## License
This project is under the Apache License 2.0 